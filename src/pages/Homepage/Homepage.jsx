
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { updateProductList } from '../../stores/actions';
import Card from '../../components/Card/card';
import Modal from '../../components/Modal/Modal';



function Homepage() {

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(updateProductList());
  }, [dispatch]);

  const products = useSelector((state) => state.products)


    return (
      <>
          <Modal text = 'The product has been successfully added to the cart!' />
            <div className="products">
              {products.map(p => <Card key = {p.id} info={p} />)}
            </div>
      </>

     );
}


export default Homepage;