import { useSelector } from "react-redux";
import Modal from "../../components/Modal/Modal";
import Product from "./CartItems";


function Cart() {


    const cart = useSelector((state) => state.cart)

    const styles = {
        fontSize: "28px",
        fontWeight: "600",
        marginBottom: "20px"
    }


    return (
        <>
        <h2 style={styles}>In your cart:</h2>
        <div className="products">
            {cart.map(p => <Product key = {p.id} info={p} />)}
        </div>
              <Modal text = 'The product has been removed from the cart!' />
        </>
     );
}





export default Cart;