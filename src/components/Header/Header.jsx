
import styles from "./Header.module.scss"
import {ReactComponent as Star} from '../../img/13487794321580594410.svg'
import {ReactComponent as Cart} from '../../img/7524495561543238919.svg'
import {ReactComponent as Home} from '../../img/1976053.svg'
import {Link} from "react-router-dom";
import logo from './3543349.jpg'
import { useSelector } from "react-redux"


function Header(){

    const favCaunter = useSelector((state) => state.favorites.length)
    const cardCaunter = useSelector((state) => state.cart.length)

    const itemFavorite = useSelector((state) => state.favorites)
    localStorage.setItem('Favorit Items', JSON.stringify( itemFavorite))

    const itemCart = useSelector((state) => state.cart)
    localStorage.setItem('Cart Items', JSON.stringify(itemCart))



    return (
        <div className={styles.header}>
            <Link to='/'>
                <div className={styles.logo}>
                    <img src={logo} alt="logo" />
                </div>
            </Link>
            <div className={styles.iconWrapper}>
            <div className={styles.wrapper}>
                <Link to='/' className={styles.link}><Home className={styles.home}/></Link>
                </div>
                <div className={styles.wrapper}>
                    <p className={styles.card__caunter}>{cardCaunter}</p>
                    <Link to='/cart' className={styles.link}><Cart className={styles.card}/></Link>
                </div>

                <div className={styles.wrapper}>
                    <p className={styles.favorite__caunter}>{favCaunter}</p>
                    <Link to='/favorite' className={styles.link}><Star className={styles.favorite}/></Link>
                </div>
            </div>
        </div>
    )
}

export default Header

